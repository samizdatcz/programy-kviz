(function() {
  function shuffle(a) {
    for (let i = a.length; i; i--) {
        let j = Math.floor(Math.random() * i);
        [a[i - 1], a[j]] = [a[j], a[i - 1]];
    }
  }

  function buildQuiz() {
    const output = [];
    shuffle(otazky)
    otazky = otazky.slice(0,10)
    otazky.forEach(function vyflusOtazky(currentQuestion, questionNumber) {
      const answers = [];

      // spravna moznost z JSONu
      moznosti = [currentQuestion.strana];

      // 3 nahodne spatne k tomu
      possibls = Object.keys(strany);
      
      while (moznosti.length < 4) {
 		possibl = possibls[possibls.length * Math.random() << 0];
      	if ($.inArray(possibl, moznosti) == -1) {
			moznosti.push(possibl);
		}
      }

      // vyflus moznosti

      moznosti.forEach(function vyflusMoznosti(moznost) {

       if (moznost == currentQuestion.strana) {
       	corr="spravna"
       } else {
       	corr="spatna"
       }

       answers.push(
          `<label class="lejbl">
            <input type="radio" name="question${questionNumber}" value="${corr}">
            ${moznost}
          </label>`
       );
      });

      // zamichani a vyflus otazek
      shuffle(answers)

      output.push(
        `<div class="question"> ${questionNumber+1}. ${currentQuestion.teze} </div>
        <div class="answers" id="q${currentQuestion.id}"> ${answers.join("")} </div>
        <div class="restext" id="r${currentQuestion.id}"></div><p>`
      );
    });

    quizContainer.innerHTML = output.join("");
  }

  const quizContainer = document.getElementById("quiz");

  const stamp = Date.now() + Math.floor(Math.random()*10000);

  numCorrect = 0;

  //vochcavka kvuli spravnemu prirazeni vyflusu statistik
  jsonid = [];
  jsonidPos = 0;  

  buildQuiz();

  $("input[type='radio']").change(function() {
  	 qid = $(this).parent().parent().attr('id')
  	 rid = "#r" + qid.slice(1,)

  	 strana = $(this).parent().text().trim()
  	 sprStrana = $("#" + qid + " [value='spravna']").parent().text().trim()
  	 pid = strany[strana]
  	 spid = strany[sprStrana]

  	 jsonid.push(rid);

  	 // znehybneni a barvicky
  	 cudliky = "[name=" + ($(this).attr('name')) + "]"
  	 $(cudliky).attr('disabled',true);

	 $(this).parent().parent().css("color", "red");
	 $("#" + qid + " [value='spravna']").parent().css("color", "green");

  	 // vyhodnoceni po otazkach
  	 if (strana == sprStrana) {
  	 numCorrect++;
  	 console.log($("#" + qid + " [value='spravna']"))
  	 $(rid).text("Správná odpověď!");
  	 $(rid).css("color", "green");

  	 } else {

  	 $(rid).text("Vedle! Teze pochází z programu strany " + sprStrana +". ");
  	 $(rid).css("color", "red");

  	 };

 	 $.ajax({
            url: "https://0oka9twq1a.execute-api.us-west-2.amazonaws.com/prod",
            type: "POST",
            crossDomain: !0,
            contentType: "application/json",
            data: JSON.stringify([stamp, qid, pid]),
            dataType: "json",
            success: function(response){

            	totalAnswers = 0;
            	correctAnswers = 0;

            	Object.keys(response).forEach(function sendOdpoved(odpoved) {
            		totalAnswers += response[odpoved]
            		if (odpoved == spid) {
            			correctAnswers = response[odpoved]
            		}
            	});

            	correctPct = Math.floor((correctAnswers / totalAnswers) * 100);

            	$(jsonid[jsonidPos]).append(" Správně odpovědělo " + correctPct + " % lidí.");

            	jsonidPos ++;
            }
        })
  })

  //vysledkovy cudlik
  $("#submit").click( function() {
  	
  	$("[type=radio]").attr('disabled',true);

 	if (numCorrect < 4 ) {
  		$("#results").text(`Trefili jste ${numCorrect} z ${otazky.length} programových tezí! Zkuste si kvíz zopakovat s jinými otázkami, třeba budete mít větší štěstí.`);
  	} else if (4 <= numCorrect < 7) {
  		$("#results").text(`Trefili jste ${numCorrect} z ${otazky.length} programových tezí! Není to špatné, ale expert na politické programy z vás ještě není. Zkuste si kvíz zopakovat s jinými otázkami!`);
  	} else if (numCorrect >= 7) {
  		$("#results").text(`Trefili jste ${numCorrect} z ${otazky.length} programových tezí! Ve volebních programech se skvěle vyznáte - nebo jste měli štěstí. Zopakujte si kvíz s jinými otázkami a zkuste dosáhnout stejného výsledku.`);
  	};

  //easter egg
  	if (document.URL.indexOf("?sendvic") !== -1 ) {
  		$("#results").append(" Kdybyste byl/a sendvič, budete ");
  		switch(numCorrect) {
  			case 0:
  				$("#results").append("English Muffin Breakfast Sandwich!<img src='http://www.laurafuentes.com/wp-content/uploads/2015/02/egg-mcmuffins-with-coffee-681x1024.jpg' width='100%'>");
  				break;
  			case 1: 
  				$("#results").append("Smoky Barbecued Beef Sandwich!<img src='https://s-media-cache-ak0.pinimg.com/736x/14/87/bc/1487bc6191229200c8bb4382d44270c4--pulled-beef-crock-pot-recipes-slow-cooker-pulled-beef-crock-pot-recipes-sandwiches.jpg' width='100%'>");
  				break;
  			case 2:
  				$("#results").append("Hot Beef Sandwich!<img src='https://images-gmi-pmc.edge-generalmills.com/339bf019-7cfa-4ad2-9dae-8a5a9f77f010.jpg' width='100%'>");
  				break;
  			case 3: 
  				$("#results").append("Ham and Cheese Stromboli!<img src='http://cf.belleofthekitchen.com/wp-content/uploads/2015/09/ham-cheese-stromboli6.jpg' width='100%'>");
  				break;
  			case 4:
  				$("#results").append("Grilled Cheese Sandwich!<img src='http://1.bp.blogspot.com/-hOisAYJTwWE/TrCPTTspz9I/AAAAAAAAM8Y/RIqSBVtAx88/s800/The%2BPerfect%2BGrilled%2BCheese%2BSandwich%2B800%2B1581.jpg' width='100%'>");
  				break;
  			case 5: 
  				$("#results").append("Grilled Reuben Sandwich! <img src='http://healthyeatingforfamilies.com/wp-content/uploads/2014/11/reuben-sandwichiStock_000005666730Medium-1024x684.jpg' width='100%'>");
  				break;
  			case 6:
  				$("#results").append("Philly Cheese Steak!<img src='http://assets.epicurious.com/photos/578e746c95824bf90525e568/master/pass/michaels-famous-philly-cheese-steak-sandwich.jpg' width='100%'>");
  				break;
  			case 7: 
  				$("#results").append("Chicken Salad Sandwich!<img src='https://images-gmi-pmc.edge-generalmills.com/e34bfb4d-7d68-4249-99f9-078c8df7d502.jpg' width='100%'>");
  				break;
  			case 8:
  				$("#results").append("Sloppy Joe!<img src='http://www.simplyrecipes.com/wp-content/uploads/2009/11/sloppy-joe-horiz-a-1600.jpg' width='100%'>");
  				break;
  			case 9: 
  				$("#results").append("Po' Boy!<img src='https://static1.squarespace.com/static/5522e704e4b09b97d697cec5/t/557f4806e4b0971f7f8c62f9/1434404871998/Untitled-3.jpg?format=2500w' width='100%'>");
  				break;
  			case 10:
  				$("#results").append("Banh mi!<img src='https://assets3.thrillist.com/v1/image/1879325/size/tl-horizontal_main.jpg' width='100%'>");
  		}
  	}
  } ) ;

  //reset cudlik
  $("#reset").click( function() {
    $('html, body').animate({
        scrollTop: $("#skrolsem").offset().top
    }, 0);  	
  	location.reload()
  } ) ;

})();
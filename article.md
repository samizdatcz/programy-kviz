title: "Vyznáte se ve stranických slibech? Vyzkoušejte si kvíz politických programů"
perex: ""
authors: ["Michal Zlatkovský"]
published: "24. července 2017"
coverimg: https://upload.wikimedia.org/wikipedia/commons/7/7d/Zasedac%C3%AD_s%C3%A1l_Poslaneck%C3%A9_sn%C4%9Bmovny.jpg
coverimg_note: "Foto <a href='https://commons.wikimedia.org/wiki/Category:Thunovsk%C3%BD_pal%C3%A1c#/media/File:Zasedac%C3%AD_s%C3%A1l_Poslaneck%C3%A9_sn%C4%9Bmovny.jpg'>Ervinpospisil/Wikimedia Commons (CC-BY-SA 2.5)</a>"
styles: []
libraries: ["https://unpkg.com/jquery@3.2.1"]
options: "" #wide
---

Poznáte politickou stranu jen podle jejích programových bodů? Zjistíte to v našem kvízu politických programů. Každý z deseti konkrétních programových bodů přiřadíte ke konkrétní straně. Hned po výběru odpovědi uvidíte, jestli jste se trefili - a kolik procent lidí odpovědělo správně.

Každý kvíz je unikátní - otázky i možnosti se náhodně vybírají z naší databáze programových tezí. V té je rovnoměrně zastoupeno deset stran s největší šancí dostat se v příštích volbách do Poslanecké sněmovny. <span id="skrolsem"></span>

<div id="quiz"></div>
<div id="results"></div>
<br>
<button id="submit" class="cudlik">Spočítat body</button> <button id="reset" class="cudlik">Chci nový kvíz!</button>

Při sbíraní tezí jsme vycházeli z nejaktuálnějších programů dostupných na webech jednotlivých politických stran. V případě strany ANO jsme vzhledem k dosud neexistujícímu programu použili knihu jejího lídra Andreje Babiše O čem sním, když náhodou spím. 

Teze jsou vždy uvedeny v co nejpřesnější verzi z konkrétního programu. V případě, že je odpověď označena jako nesprávná, neznamená to, že daná strana nenavrhuje podobné či stejné opatření - jako správná se však zobrazuje jen ta strana, která má ve svém programu přímo konkrétní uvedenou formulaci.